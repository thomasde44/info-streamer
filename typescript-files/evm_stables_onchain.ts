
const ethers = require('ethers');  


/*
--------------------------------------------------------------
	tether usdc dai
--------------------------------------------------------------
*/
const eth_tokens = ['0xdac17f958d2ee523a2206206994597c13d831ec7', '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48', '0x6b175474e89094c44da98b954eedeac495271d0f'];
const weth = '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2';
/*
--------------------------------------------------------------
	routers and factories of sushi, uniswap v2
--------------------------------------------------------------
*/
const router__address = ['0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F', '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D']
const factory_addresses = ['0xC0AEe478e3658e2610c5F7A4A2E1777cE9e4f2Ac', '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f']

/*
--------------------------------------------------------------
    number of routers for the j indexed for loop get_price
--------------------------------------------------------------
*/
const number_of_routers = 2;

/*
--------------------------------------------------------------
    use json file for access to function
--------------------------------------------------------------
*/
const tokenABI = require('./erc20.json');

/*
--------------------------------------------------------------
    smart contract function templates router contracts
--------------------------------------------------------------
*/
const routerABI_eth =  [
    'function getAmountsOut(uint amountIn, address[] calldata path) external view returns (uint[] memory amounts)',
    'function quote(uint amountA, uint reserveA, uint reserveB) external pure returns (uint amountB)',
    'function price0CumulativeLast() external view returns (uint)',
    'function price1CumulativeLast() external view returns (uint)',
    'function swapExactTokensForTokens(uint amountIn,uint amountOutMin,address[] calldata path,address to,uint deadline) external returns (uint[] memory amounts)'
]
/*
--------------------------------------------------------------
    arbitrum smart contract template
--------------------------------------------------------------
*/
const pairABI_arb = [
    'function getReserves() external view returns (uint112 reserve0, uint112 reserve1, uint32 blockTimestampLast)',
]

/*
--------------------------------------------------------------
    evm pair specific function templates
--------------------------------------------------------------
*/
const pairABI_eth = ['event Swap(address indexed sender,uint amount0In,uint amount1In,uint amount0Out,uint amount1Out,address indexed to)',
'function getReserves() public view returns (uint112 _reserve0, uint112 _reserve1, uint32 _blockTimestampLast)',
'function getPair(address tokenA, address tokenB) external view returns (address pair)'
]


/*
--------------------------------------------------------------
    create rpc endpoint for ethereum node
    init wallet so you can sign transactions
--------------------------------------------------------------
*/
const provider_eth = new ethers.providers.JsonRpcProvider('https://mainnet.infura.io/v3/');
var wallet = new ethers.Wallet.fromMnemonic('wallet mnemonic');
const signer_eth = wallet.connect(provider_eth);




/*
--------------------------------------------------------------
	make a contract to interact with
    params: contracts address, contracts abi, signer
    returns: contract object
--------------------------------------------------------------
*/
const make_contract = async (address, abi, sign) => {
    try {
        const contract = new ethers.Contract(address, abi, sign);
        return contract;
    } catch(error) {
        console.log(error);
    }
}


/*
--------------------------------------------------------------
	Get price of the stable tokens from respective exchanges
    it loops over each router one for sushi and one for uni.
    Then loops over each of the pair address token/weth.
    params: none
    returns: console pipes input into the go variable from
             its calling go process in cli_eth_stables.go
--------------------------------------------------------------
*/
const get_price = async () => {
    
    for (let j = 0; j < number_of_routers; j++) {
        for (let i = 0; i < eth_tokens.length; i++) {
            try {


                const pairEth = await make_contract(factory_addresses[j], pairABI_eth, signer_eth);
                const pair_addr = await pairEth.getPair(eth_tokens[i], weth);
                
                const pair = await make_contract(pair_addr, pairABI_eth, signer_eth);
                const routerContract = await make_contract(pair_addr, routerABI_eth, signer_eth);

                const tk0 = await make_contract(weth, tokenABI, signer_eth);
                const tk1 = await make_contract(eth_tokens[i], tokenABI, signer_eth);
                
            
                const dec0 = await tk0.decimals();
                const dec1 = await tk1.decimals();
                const sym = await tk0.symbol();
                const sym1 = await tk1.symbol();
        
                const result = await pair.getReserves();
    
                if (result[0].toString().length < result[1].toString().length) {
                    if (dec0 > dec1) {
                        var token_price = ((result[0]/10**dec1)/(result[1]/10**dec0));
                        var weth_price = ((result[1]/10**dec0)/(result[0]/10**dec1))
                    }
                } else {
                    var weth_price = ((result[0]/10**dec0)/(result[1]/10**dec1));
                    var token_price = ((result[1]/10**dec1)/(result[0]/10**dec0))
                }

                console.log(weth_price, token_price, (weth_price*token_price), result[2], router__address[j]);
               
            } catch(error) {
                console.log(error);
            }
        }
    }
}

get_price();




