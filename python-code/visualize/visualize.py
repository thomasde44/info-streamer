from elasticsearch import Elasticsearch
import pandas
import matplotlib.pyplot as plt
es = Elasticsearch(
    ['http://localhost:9200'],
    verify_certs=False,
    ssl_show_warn=False,
    http_auth=("username", "password"),
)



a = es.search(index="binance-open-interest", size=10000, query={"match_all": {}}, sort={"Time": {"order": "desc"}})
b = []
for i in a['hits']['hits']:
    b.append(i['_source']['OpenInterest'])
    # print(i['_source']['OpenInterest'])
print(len(b))

query2 = {
    "bool": {
        "timestamp": {
            "lte": "now/s",
        }
    }
}

c = es.search(index="binance-price", size=10000, query={"match_all": {}}, sort={"Time": {"order": "desc"}})
d = []
for i in c['hits']['hits']:
    d.append(i['_source']['Price'])
    # print(i['_source']['OpenInterest'])
print(len(d))

query3 = {
    "bool": {
        "timestamp": {
            "lte": "now/s",
        }
    }
}

e = es.search(index="bitfinex-open-interest", size=10000, query={"match_all": {}}, sort={"Time": {"order": "desc"}})
f = []
for i in e['hits']['hits']:
    f.append(i['_source']['OpenInterest'])
    # print(i['_source']['OpenInterest'])
print(len(f))

# df = pandas.DataFrame(b)

ts = pandas.Series(b)
ts2 = pandas.Series(d)
ts3 = pandas.Series(f)
plt.figure(1)
plt.subplot(311).set_title('open interest binance')
plt.plot(ts)
plt.subplot(312).set_title('price')
plt.plot(ts2)
plt.subplot(313).set_title('open interest bitfinex')
plt.plot(ts3)

plt.show()
# ts.plot()
# ts2.plot()
# plt.show() 
