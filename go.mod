module info-streamer

go 1.17

require github.com/elastic/go-elasticsearch/v8 v8.0.0-20211214143643-cd88bcfe6133

require (
	code.cryptowat.ch/clock v0.0.0-20190102160946-e933bd4e0935 // indirect
	code.cryptowat.ch/cw-sdk-go v1.0.2 // indirect
	github.com/PullRequestInc/go-gpt3 v1.1.4 // indirect
	github.com/cenkalti/backoff/v4 v4.1.2 // indirect
	github.com/coreos/pkg v0.0.0-20180928190104-399ea9e2e55f // indirect
	github.com/dghubble/go-twitter v0.0.0-20211115160449-93a8679adecb // indirect
	github.com/dghubble/oauth1 v0.7.0 // indirect
	github.com/dghubble/sling v1.4.0 // indirect
	github.com/elastic/elastic-transport-go/v8 v8.0.0-20211202110751-50105067ef27 // indirect
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/google/uuid v1.1.1 // indirect
	github.com/gorilla/websocket v0.0.0-20180605202552-5ed622c449da // indirect
	github.com/juju/errors v0.0.0-20210818161939-5560c4c073ff // indirect
	github.com/vartanbeno/go-reddit/v2 v2.0.1 // indirect
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7 // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d // indirect
	google.golang.org/appengine v1.4.0 // indirect
	gopkg.in/yaml.v2 v2.2.4 // indirect
	honnef.co/go/tools v0.0.0-20190102054323-c2f93a96b099 // indirect
	rogchap.com/v8go v0.7.0 // indirect
)
