package main

import "reflect"
import "testing"





func Test_4chan(t *testing.T) {
	resp, err := get_json_http_resp("https://a.4cdn.org/pol/catalog.json")
	if err != nil{
		t.Errorf("error: %v", err)
	}
	_, err2 := decode_catalog_resp(resp)
	if err2 != nil{
		t.Errorf("error: %v", err)
	}
}


func Test_elastic(t *testing.T) {
	cli := make_es_client()
	
	var t1 = create_index(cli, "test_index");
	if t1.StatusCode != 200{
		t.Errorf("error: %v", t1.StatusCode)
	} 
	var t2 = delete_index(cli, "test_index");
	if t2.StatusCode != 200{
		t.Errorf("error: %v", t2.StatusCode)
	}
}

func Test_markets(t *testing.T) {
	val := bitfinex_open_interest("tBTCF0:USTF0")
	var num float64;
	t1 := reflect.TypeOf(val)
	t2 := reflect.TypeOf(num)
	if t1 != t2{ 
		t.Errorf("error: %v", val)
	}
}