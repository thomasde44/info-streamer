package main

import (
  "encoding/json"
  "strconv"
  "sync"
  "fmt"
  "strings"
  "time"
  "github.com/elastic/go-elasticsearch/v8"
)

/* 
--------------------------------------------------------------
  indexes for database
--------------------------------------------------------------
*/
const binance_open_interest_ = "binance-open-interest"
const bitfinex_open_interest_ = "bitfinex-open-interest"
const binance_price_ = "binance-price"
const binance_kline_btcusdt_ = "binance-kline-btcusdt"
const binance_kline_ethbtc_ = "binance-kline-ethbtc"




/* 
--------------------------------------------------------------
get bitfinex open interest value 
Param: string for symbol pair for exchange 
Return: *esapi.Response
Example: bitfinex_open_interest("tBTCF0:USTF0")
--------------------------------------------------------------
*/
func bitfinex_open_interest(keys string) (float64) {
  tmp := "https://api-pub.bitfinex.com/v2/status/deriv?keys=" + keys
  res, err := query_get_string(tmp)
  if err != nil {
    panic(err)
  }
  if res != "" {
    res = res[1:len(res)-1]
    res = res[1:len(res)-1]
    res = res[0:len(res)-1]
    res = res[0:len(res)-1]
    s := strings.Split(res, ",")
    val, err := strconv.ParseFloat(s[18], 64)
    if err != nil {
      panic(err)
    }
    return val
  } else {
    return 0
  }

}
/* 
--------------------------------------------------------------
get bitfinex open interest history value 
ex params: keys = "tBTCF0:USTF0" 
return: string of comma separated values     
Example: bitfinex_open_interest_history("start=157057800000&end=1573566992000&limit=10&sort=-1")   
--------------------------------------------------------------
*/
func bitfinex_open_interest_history(keys string) (float64) {
  tmp := "https://api-pub.bitfinex.com/v2/status/deriv/tBTCF0:USTF0/hist?" + keys
  res, err := query_get_string(tmp)
  if err != nil {
    panic(err)
  }
  
  if res != "" {
    res = res[1:len(res)-1]
    res = res[1:len(res)-1]
    res = res[0:len(res)-1]
    res = res[0:len(res)-1]
    s := strings.Split(res, ",")
    val, err := strconv.ParseFloat(s[len(s)-6], 64)
    if err != nil {
      panic(err)
    }
    return val
  } else {
    return 0
  }
}

/* 
--------------------------------------------------------------
struct to store binance rest result 
--------------------------------------------------------------
*/
type binance_oi struct {
  Symbol string //`json:"symbol"`
  OpenInterest string //`json:"openInterest"`
  Time int64 //`json:"time"`
} 
/* 
--------------------------------------------------------------
get open interest value from binance 
ex params: keys = "tBTCF0:USTF0"
return: string (open interest value)
Example: binance_open_interest("BTCUSDT").OpenInterest
--------------------------------------------------------------
*/
func binance_open_interest(keys string) (float64) {
  var binance_struct binance_oi
  tmp := "https://www.binance.com/fapi/v1/openInterest?symbol=" + keys
  res, err := query_get_string(tmp)
  if err != nil {
    panic(err)
  }
  json.Unmarshal([]byte(res), &binance_struct)
  val, err := strconv.ParseFloat(binance_struct.OpenInterest, 64)
  if err != nil {
    panic(err)
  }

  return val
}



/*
--------------------------------------------------------------
get ftx candle data
param: string (symbol to concat to url string)
returns: void
@TODO
--------------------------------------------------------------
*/
func get_ftx_candle(symbol string) {

  // var ftx_struct []ftx_candle
  // https://ftx.com/api
  tmp := "https://ftx.com/api/markets/" + symbol + "/candles?resolution=60"
  _, err := query_get_string(tmp)
  if err != nil {
    panic(err)
  }
  
}

/*
--------------------------------------------------------------
struture to store the kline information from binance
--------------------------------------------------------------
*/
type binance_kline struct {
  Open_time time.Time 
  Open float64 
  High float64 
  Low float64 
  Close float64  
  Volume float64 
  Close_time time.Time 
  Quote_asset_volume float64 
  Number_of_trades int64 
  Taker_buy_base_asset_volume float64 
  Taker_buy_quote_asset_volume float64
  Ignore float64 
}

/*
--------------------------------------------------------------
  params: ticker string, interval string
  returns: string of json binance kline type
  example: binance_kline("BTCUSDT", "1h")
--------------------------------------------------------------
*/
func get_binance_kline(symbol string, freq string) ([]binance_kline) {
  var binance_struct []binance_kline
  tmp := "https://api.binance.com/api/v1/klines?symbol=" + symbol + "&interval=" + freq
  res, err := query_get_string(tmp)
  if err != nil {
    fmt.Println("fuck")
    panic(err)
  }
  json.Unmarshal([]byte(res), &binance_struct)
  res = res[1:len(res)-1]
  res = res[1:len(res)-1]
  res = res[0:len(res)-1]
  
  res_2 := strings.Split(res, "],[")

  for i := 0; i < len(res_2); i++ {
    res_3 := strings.Split(res_2[i], ",")
    for j := 0; j < len(res_3); j++ {
      res_3[j] = strings.Replace(res_3[j], "\"", "", -1)
    }
    
    j, err := strconv.ParseInt(res_3[0], 10, 64)
    if err != nil {
      fmt.Println("fuck")
        panic(err)
    }
    k, err := strconv.ParseInt(res_3[6], 10, 64)
    if err != nil {
      fmt.Println("fuck")
        panic(err)
    }
    binance_struct[i].Open_time = time.Unix(j/1000, 0)
    binance_struct[i].Open, _ = strconv.ParseFloat(res_3[1], 32)
    binance_struct[i].High, _ = strconv.ParseFloat(res_3[2], 64)
    binance_struct[i].Low, _ = strconv.ParseFloat(res_3[3], 64)
    binance_struct[i].Close, _ = strconv.ParseFloat(res_3[4], 64)
    binance_struct[i].Volume, _ = strconv.ParseFloat(res_3[5], 64)
    binance_struct[i].Close_time = time.Unix(k/1000, 0)
    binance_struct[i].Quote_asset_volume, _ = strconv.ParseFloat(res_3[7], 64)
    binance_struct[i].Number_of_trades, _ = strconv.ParseInt(res_3[8], 10, 64)
    binance_struct[i].Taker_buy_base_asset_volume, _ = strconv.ParseFloat(res_3[9], 64)
    binance_struct[i].Taker_buy_quote_asset_volume, _ = strconv.ParseFloat(res_3[10], 64)
    binance_struct[i].Ignore, _ = strconv.ParseFloat(res_3[11], 64)
  }


  return binance_struct

}


/*
--------------------------------------------------------------
  struct to store binance rest result 
--------------------------------------------------------------
*/
type binance_ticker struct {
  Symbol string 
  Price string 
  Time int64 
} 

/*
--------------------------------------------------------------
  get binance price ticker
  params: keys = "tBTCF0:USTF0" (ex)
  return: float64 (price)
  Example: binance_price("BTCUSDT")
--------------------------------------------------------------
*/
func binance_price(keys string) (float64) {
  var binance_struct binance_ticker
  tmp := "https://www.binance.com/fapi/v1/ticker/price?symbol=" + keys
  res, err := query_get_string(tmp)
  if err != nil {
    panic(err)
  }
  json.Unmarshal([]byte(res), &binance_struct)
  val, err := strconv.ParseFloat(binance_struct.Price, 64)
  if err != nil {
    panic(err)
  }
  return val
}





/*
--------------------------------------------------------------
  stores open interest docs at the indices all the open interest related indices 
  Param: *elasticsearch.Client
  Return: *esapi.Response
  Example: store_open_interest_document(cli)
--------------------------------------------------------------
*/
func store_open_interest_document(cli *elasticsearch.Client) {
  var	wg sync.WaitGroup
  var fns []func(string) float64
  fns = append(fns, bitfinex_open_interest)
  fns = append(fns, binance_open_interest)
  var keys = []string{"tBTCF0:USTF0", "BTCUSDT"}
  var indices = []string{bitfinex_open_interest_, binance_open_interest_}
  
  for i, fn := range fns {

    wg.Add(1)
    go func (cli *elasticsearch.Client, i int) { 
      defer wg.Done()
      f := keys[i]
      indx := indices[i]
      s := fn(f)
      doc := open_interest_doc{}
      doc.Time = time.Now()
      doc.OpenInterest = s
      store_doc(doc, indx, cli)
      
    } (cli, i) 
    wg.Wait()
  }
}


/*
--------------------------------------------------------------
  params: *elasticsearch.Client
  returns: void
  example: store_doc(doc, "bitfinex_open_interest_", cli)
--------------------------------------------------------------
*/
func store_binance_kline_document(cli *elasticsearch.Client) {
  var	wg sync.WaitGroup
  var keys = []string{"BTCUSDT", "ETHUSDT"}
  var indices = []string{binance_kline_btcusdt_, binance_kline_ethbtc_}
  var thread_store_indices = []string{"threads-store-bnance-btcusdt", "threads-store-bnance-ethbtc"}
  
  for i, _ := range keys {

    wg.Add(1)
    go func (cli *elasticsearch.Client, i int) { 
      defer wg.Done()
      f := keys[i]
      indx := indices[i]
      srch_indx := thread_store_indices[i]
      s := get_binance_kline(f, "1m")
      for i := 0; i < len(s); i++ {

        if search_threads(cli, s[i].Open_time.Unix(), srch_indx) == false {
          store_thread_no(s[i].Open_time.Unix(), srch_indx, cli)
          doc := binance_kline{}
          doc.Open_time = s[i].Open_time
          doc.Open = s[i].Open
          doc.High = s[i].High
          doc.Low = s[i].Low
          doc.Close = s[i].Close
          doc.Volume = s[i].Volume
          doc.Close_time = s[i].Close_time
          doc.Quote_asset_volume = s[i].Quote_asset_volume
          doc.Number_of_trades = s[i].Number_of_trades
          doc.Taker_buy_base_asset_volume = s[i].Taker_buy_base_asset_volume
          doc.Taker_buy_quote_asset_volume = s[i].Taker_buy_quote_asset_volume
          doc.Ignore = s[i].Ignore
          store_doc(doc, indx, cli)

      } 
    }
      
    } (cli, i)
    wg.Wait()
  }

}


/*
--------------------------------------------------------------
  params: *elasticsearch.Client
  returns: void
  example: store_doc(doc, "bitfinex_open_interest_", cli)
--------------------------------------------------------------
*/
func store_exchange_candle_data(cli *elasticsearch.Client) {
  var	wg sync.WaitGroup
  var fns []func(*elasticsearch.Client) 
  fns = append(fns, store_binance_kline_document)

  for i := 0; i < len(fns); i++ {
    wg.Add(1)
    go func (cli *elasticsearch.Client, i int) { 
      defer wg.Done()
      fns[i](cli)
    } (cli, i)
    wg.Wait()
  }
} 


/*
--------------------------------------------------------------
  store price in elasticsearch 
  Param: *elasticsearch.Client
  Return: void
  Example: store_price_documents(cli)
--------------------------------------------------------------
*/
func store_price_document(cli *elasticsearch.Client) {
  doc := price_doc{}
  doc.Time = time.Now()
  doc.Price = binance_price("BTCUSDT")
  store_doc(doc, "binance-price", cli)
}

