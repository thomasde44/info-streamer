package main

import (
	"time" 

	"github.com/elastic/go-elasticsearch/v8"
)

/*
--------------------------------------------------------------
	open interest docs being stored
--------------------------------------------------------------
*/
func interval_store_open_interest(cli *elasticsearch.Client){
	
	ticker := time.NewTicker(1 * time.Second * 3)
	for _ = range ticker.C {
		store_open_interest_document(cli)
	}
}
/*
--------------------------------------------------------------
	tweet docs being stored
--------------------------------------------------------------
*/
func interval_store_tweets(cli *elasticsearch.Client){
	
	ticker := time.NewTicker(1 * time.Second * 180)
	for _ = range ticker.C {
		store_tweet_document(cli)
	}	
}
/*
--------------------------------------------------------------
	btc/usdt price docs being stored
--------------------------------------------------------------
*/
func interval_store_price(cli *elasticsearch.Client){
	
	ticker := time.NewTicker(1 * time.Second * 5)
	for _ = range ticker.C {
		store_price_document(cli)
	}
}
/*
--------------------------------------------------------------
	4chan docs being stored
--------------------------------------------------------------
*/
func interval_store_4chan(cli *elasticsearch.Client){
	ticker := time.NewTicker(1 * time.Second * 120)
	for _ = range ticker.C {
		store_4chan_doctument(cli)
	}	
}
/*
--------------------------------------------------------------
	reddit docs being stored
--------------------------------------------------------------
*/
func interval_store_reddit(cli *elasticsearch.Client){
	
	ticker := time.NewTicker(1 * time.Second * 120)
	for _ = range ticker.C {
		store_reddit_doctument(cli)
	}
}
/*
--------------------------------------------------------------
	binance min interval docs being stored
--------------------------------------------------------------
*/
func interval_store_binance_kline(cli *elasticsearch.Client){
	
	ticker := time.NewTicker(1 * time.Second * 60)
	for _ = range ticker.C {
		store_binance_kline_document(cli)
	}
}
/*
--------------------------------------------------------------
	sushi and uni stable coin docs being stored
--------------------------------------------------------------
*/
func interval_store_stables_sushi_eth(cli *elasticsearch.Client){
	ticker := time.NewTicker(1 * time.Second * 60)
	for _ = range ticker.C {
		store_stables_sushi_eth_document(cli)
	}
}


func main() {

/*
--------------------------------------------------------------
	clients
--------------------------------------------------------------
*/
	cli := make_es_client()
	cli2 := make_es_client()
	cli3 := make_es_client()
	cli4 := make_es_client()
	cli5 := make_es_client()
	cli6 := make_es_client()
	cli7 := make_es_client()

/*
--------------------------------------------------------------
	async collection
--------------------------------------------------------------
*/
	for {
		go interval_store_tweets(cli)
		go interval_store_open_interest(cli2)
		go interval_store_binance_kline(cli3)
		go interval_store_price(cli4)
		go interval_store_4chan(cli5)
		go interval_store_reddit(cli6)
		go interval_store_stables_sushi_eth(cli7)
		select {}
	}
  
}