package main


import (
	"os/exec"
	"strings"
	"strconv"
	"time"
	"sync"
	"github.com/elastic/go-elasticsearch/v8"
	
	
)

/*
--------------------------------------------------------------
	params: *elasticsearch.Client
	returns: void
	purpose: stores the sushi doc in the sushi-stables-eth-usdt index
--------------------------------------------------------------
*/
func store_stables_sushi_eth_document(cli *elasticsearch.Client) {

	data, err := exec.Command("node", "--openssl-legacy-provider", "/home/demon/collector/typescript-files/evm_stables_onchain.ts").Output()
	if err != nil {
		panic(err)
	}

	output2 := string(data)
	output3 := strings.Split(output2, "\n")

	indices := []string{"sushi-stables-eth-usdt", "sushi-stables-eth-usdc", "sushi-stables-dai", "uni-stables-eth-usdt", "uni-stables-eth-usdc", "uni-stables-dai"}
	var	wg sync.WaitGroup
	
	for i, index := range indices {
		wg.Add(1)

		go func(cli *elasticsearch.Client, i int, index string) {
			defer wg.Done()
			tmp := strings.Split(output3[i], " ")
			doc := Stable_doc{}
			doc.Token_price, _ = strconv.ParseFloat(tmp[0], 64)
			doc.Weth_price_usd, _ = strconv.ParseFloat(tmp[1], 64)
			doc.Dollar_price_usd, _ = strconv.ParseFloat(tmp[2], 64)
			j, err := strconv.ParseInt(tmp[3], 10, 64)
			if err != nil {
				panic(err)
			}
			doc.Time = time.Unix(j, 0)
			doc.Router_address = tmp[4]
			store_doc(doc, index, cli)
		}(cli, i, index)
		wg.Wait()
	}
	
}
