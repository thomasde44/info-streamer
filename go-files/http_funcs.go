package main

import (
  "io"
  "net/http"
)

/*
--------------------------------------------------------------
	params: url string, header http.Header
	return: *http.Response, error
	example: do_http_get("https://api.binance.com/api/v1/ticker/24hr?symbol=BTCUSDT", header)
--------------------------------------------------------------
*/
func do_http_post(url string, header http.Header) (*http.Response, error) {
	
	client := &http.Client{
	}
	req, err := http.NewRequest("POST", url, nil)
	if err != nil {
		panic(err)
	}

	req.Header = header
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	return resp, err
}
/*
--------------------------------------------------------------
	param: url string, header http.Header
	return: *http.Response, error
--------------------------------------------------------------
*/

func do_http_get(url string, header http.Header) (*http.Response, error) {

	client := &http.Client{
	}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		panic(err)
	}
	
	req.Header = header
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	return resp, err
}

/* 
--------------------------------------------------------------
	make an http get query 
	Param: url string
	Return: string of response
	Example: query_get("https://api.binance.com/api/v1/ticker/24hr?symbol=BTCUSDT")
--------------------------------------------------------------
*/
func query_get_string(url string) (string, error) {
	var netClient = &http.Client{}
	tr := &http.Transport{
        MaxIdleConns:       20,
        MaxIdleConnsPerHost:  20,
    }
    netClient = &http.Client{Transport: tr}
	resp, err := netClient.Get(url)
	if err != nil {
	  panic(err)
	}
	defer resp.Body.Close()
	if (resp.StatusCode == 200 && resp != nil ) {
	
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}
		return string(body), nil
	} else {
		return string(""), nil
	}
  
  }



