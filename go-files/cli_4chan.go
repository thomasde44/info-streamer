package main

import (
	"fmt"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"os"
	"strconv"
	"sync"
	"time"
	"github.com/elastic/go-elasticsearch/v8"

)


/*
--------------------------------------------------------------
	specific board id constants
--------------------------------------------------------------
*/


/* 
--------------------------------------------------------------
	thread struct used in responses from server
--------------------------------------------------------------
*/
type Thread struct {
	No int `json:"no"`
	Last_Modified int `json:"last_modified"`
	Replies int `json:"replies"`
}

/*
--------------------------------------------------------------
	wrapper struct for thread response
--------------------------------------------------------------
*/ 
type Thread_Resp struct {
	Page int `json:"page"`
	Threads []Thread `json:"threads"`
}

/*
--------------------------------------------------------------
	Catalog response structure
--------------------------------------------------------------
*/ 
type Catalog_Thread struct {
	No int `json:"no"`
	Resto int `json:"resto"`
	Sticky int `json:"sticky"`
	Closed int `json:"closed"`
	Now string `json:"now"`
	Time int `json:"time"`
	Name string `json:"name"`
	Trip string `json:"trip"`
	Id string `json:"id"`
	Capcode string `json:"capcode"`
	Country string `json:"country"`
	Country_Name string `json:"country_name"`
	Sub string `json:"sub"`
	Com string `json:"com"`
	Tim int `json:"tim"`
	Filename string `json:"filename"`
	Ext string `json:"ext"`
	Fsize int `json:"fsize"`
	Md5 string `json:"md5"`
	W int `json:"w"`
	H int `json:"h"`
	Tn_W int `json:"tn_w"`
	Filedeleted int `json:"filedeleted"`
	Spoiler int `json:"spoiler"`
	Custom_Spoiler int `json:"custom_spoiler"`
	Omitted_Posts int `json:"omitted_posts"`
	Omitted_Images int `json:"omitted_images"`
	Replies int `json:"replies"`
	Images int `json:"images"`
	Bumplimit int `json:"bumplimit"`
	Imagelimit int `json:"imagelimit"`
	Last_Modified int `json:"last_modified"`
	Tag string `json:"tag"`
	Semantic_Url string `json:"semantic_url"`
	Since4pass string `json:"since4pass"`
	Unique_Ips int `json:"unique_ips"`
	M_Img int `json:"m_img"`
	Last_Replies []Catalog_Thread `json:"last_replies"`
}


/*
--------------------------------------------------------------
	struct to hold resp from https://a.4cdn.org/board-name/catalog.json
--------------------------------------------------------------
*/  
type Catalog_Resp struct {
	Page int `json:"page"`
	Catalog_Threads []Catalog_Thread `json:"threads"`
}




/*
--------------------------------------------------------------
	http.get request 
	Params: url string
	Returns: []byte
	Example: get_resp("https://a.4cdn.org/board-name/catalog.json")
--------------------------------------------------------------
*/
func get_json_http_resp(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil{
		fmt.Println("error: ", err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil{
		fmt.Println("error: ", err)
	}
	return body, err
}



/*
--------------------------------------------------------------
	decode the response from get_json_http_resp()
	Params: []byte (response from get_json_http_resp())
	Returns: []Catalog_Thread
	Example: decode_catalog_resp(get_json_http_resp("https://a.4cdn.org/board-name/catalog.json"))
	not being used
--------------------------------------------------------------
*/
func decode_thread_list_resp(resp []uint8) []Thread_Resp {

	var thread_resp []Thread_Resp
	err := json.Unmarshal([]byte(resp), &thread_resp)
	if err != nil{
		fmt.Println("error: ", err)
	}
	return thread_resp
}

/*
--------------------------------------------------------------
	create of append to file if not created 
	params: byte contect of file
	returns: void
	example: append_or_create_file([]byte("file contents"));
	not being used
--------------------------------------------------------------
*/
func append_or_create_file(content []byte) {
	fmt.Println("write fn")
	f, err := os.OpenFile("daily_threads.txt", os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil{
		fmt.Println("error: ", err)
	}
	fmt.Println("about to write brah")
	f.Write(content)
	f.Close()

}

/*
--------------------------------------------------------------
	decode catalog resp
	params: byte contect of file
	returns: Catalog_Resp[] filled in from the server
	example: append_or_create_file([]byte("file contents"));
	not being used
--------------------------------------------------------------
*/
func decode_catalog_resp(resp []uint8) ([]Catalog_Resp, error) {
	var catalog_resp []Catalog_Resp
	err := json.Unmarshal([]byte(resp), &catalog_resp)
	if err != nil{
		fmt.Println("error: ", err)
	}
	return catalog_resp, err
}


/*
--------------------------------------------------------------
	stores open interest docs at the indices all the open interest related indices  
	Param: catalog []Catalog_Resp, index string, cli *elasticsearch.Client
	Return: void
	Example: store_open_interest_documents(cli)
--------------------------------------------------------------
*/

func record_thread_from_catalog(catalog []Catalog_Resp, index string, cli *elasticsearch.Client, board string) {
	for i := range catalog {
		for j := range catalog[i].Catalog_Threads {
			if search_threads(cli, int64(catalog[i].Catalog_Threads[j].No), "threads-store-4chan") == false {
				// fmt.Println("should append")
				if catalog[i].Catalog_Threads[j].Replies > 5 {
					store_thread_no(int64(catalog[i].Catalog_Threads[j].No), "threads-store-4chan", cli)
					doc := doc_4chan{}
					doc.Title = catalog[i].Catalog_Threads[j].Com
					doc.Link = "https://boards.4channel.org/"+board+"/thread/" + strconv.Itoa(catalog[i].Catalog_Threads[j].No)
					doc.Replies = catalog[i].Catalog_Threads[j].Replies
					doc.Time = time.Now()
					store_doc(doc, index, cli)
					// fmt.Println(res)
				}
			} 
		}
	}
}

/*
--------------------------------------------------------------
	params: cli *elasticsearch.Client
	returns: void
	example: store_open_interest_documents(cli)
--------------------------------------------------------------
*/

func store_4chan_doctument(cli *elasticsearch.Client) {
	var	wg sync.WaitGroup
	var urls = []string{}
	var index_names = []string{}
	var boards = []string{}
	// get_json_http_resp("https://a.4cdn.org/biz/catalog.json")
	for i, index := range index_names {
		wg.Add(1)
		resp, err := get_json_http_resp(urls[i])
		// fmt.Println(err == nil)
		if err == nil {
			catalog, err := decode_catalog_resp(resp)
			if err != nil {
				fmt.Println("error: ", err)
			}
			go func(cli *elasticsearch.Client, i int, index string) {
				defer wg.Done()
				record_thread_from_catalog(catalog, index, cli, boards[i])
			}(cli, i, index)
			wg.Wait()
		}
	}
}


