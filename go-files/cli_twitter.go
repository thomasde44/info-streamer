package main

import (
	"fmt"
    "github.com/dghubble/go-twitter/twitter"
    "github.com/dghubble/oauth1"
	"time"
	"github.com/elastic/go-elasticsearch/v8"
	"github.com/PullRequestInc/go-gpt3"
	"context"
)

/*
--------------------------------------------------------------
	like all tweets from fetched timeline
	not using this function
--------------------------------------------------------------
*/
func like_tweet(pk string, sk string, bt string, ats string, twt twitter.Tweet) {

	config := oauth1.NewConfig(pk, sk)
	token := oauth1.NewToken(bt, ats)
	httpClient := config.Client(oauth1.NoContext, token)

	client := twitter.NewClient(httpClient)

	tweets, _, err := client.Timelines.HomeTimeline(&twitter.HomeTimelineParams{
		Count: 1000,
	})

	if err != nil {
		fmt.Println(err)
	}
	for _, tweet := range tweets {
		params := &twitter.FavoriteCreateParams{ID: tweet.ID}
		_, _, err := client.Favorites.Create(params)
		if err != nil {
			fmt.Println(err)
		}
	}

}


/*
--------------------------------------------------------------
	get client to query for an account
	params: private key, secret key, bearer token, access token secret
	returns: *twitter.Client
--------------------------------------------------------------
*/
func account_client(pk string, sk string, bt string, ats string) *twitter.Client {
	config := oauth1.NewConfig(pk, sk)
	token := oauth1.NewToken(bt, ats)
	httpClient := config.Client(oauth1.NoContext, token)
	client := twitter.NewClient(httpClient)
	return client
}


/*
--------------------------------------------------------------
	parmas: none
	returns: *twitter.Client
	get a client for designated main account
--------------------------------------------------------------
*/
func account_main_client() *twitter.Client  {
	config := oauth1.NewConfig("token", "token_secret")
	token := oauth1.NewToken("token", "token_secret")
	httpClient := config.Client(oauth1.NoContext, token)
	client := twitter.NewClient(httpClient)
	return client
}



/*
--------------------------------------------------------------
store the timeline in a struct into the es database
params: *elasticsearch.Client
returns: void (prints result of whether successful storage)
example store_tweet_document(cli)
--------------------------------------------------------------
*/
func store_tweet_document(cli *elasticsearch.Client) {

	twt_cli := account_main_client()

	twts := get_time_line(twt_cli)
	var url string
	for _, twt := range twts {
		if search_threads(cli, twt.ID, "threads-store-twitter") == false && twts != nil {
			store_thread_no(int64(twt.ID), "threads-store-twitter", cli)
			doc := tweet_doc{}
			doc.User_name = twt.User.Name
			doc.User_screen_name = twt.User.ScreenName
			doc.Tweet_Text = twt.Text
			doc.Tweet_created_at = twt.CreatedAt
			url = "https://twitter.com/" + doc.User_screen_name + "/status/" + twt.IDStr
			doc.Url = url
			doc.Time = time.Now()
			store_doc(doc, "twitter-tweets", cli)

		}
	}
}


/* 
--------------------------------------------------------------
get time line of client inputted count param in the params config is the size of the timeline
params: *twitter.Client
return: []Tweet
example: get_time_line(cli)
--------------------------------------------------------------
*/
func get_time_line(cli *twitter.Client) ([]twitter.Tweet) {

	tweets, resp, err := cli.Timelines.HomeTimeline(&twitter.HomeTimelineParams{
		Count: 1000,
	})
	if err != nil {
		fmt.Println(err)
	}

	if resp.StatusCode == 200 {
		return tweets
	} else {
		return nil
	}

}


/*
--------------------------------------------------------------
respond to tweets using gpt openai api from second account
params: *twitter.Client
returns: void
example: respond_to_time_line(cli);
--------------------------------------------------------------
*/
func respond_to_time_line(cli *twitter.Client) {

	ctx := context.Background()
	client := gpt3.NewClient("token")

	tweets, resp, err := cli.Timelines.HomeTimeline(&twitter.HomeTimelineParams{
		Count: 1000,
	})
	if err != nil {
		fmt.Println(err)
	}
	text_str := ""
	cnt := 0
	if resp.StatusCode == 200 {
		for _, tweet := range tweets {
			text_str = text_str + tweet.Text + " "
			cnt++
			if cnt == 10 {
				break
			}
		}
	}
	res, err := client.Completion(ctx, gpt3.CompletionRequest{
		Prompt: []string{text_str},
	})
	if err != nil {
		panic(err)
	} 
	fmt.Print(res.Choices[0].Text)
}

