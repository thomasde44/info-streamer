package main

import (
	"sync"
	"context"
	"log"
	"github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esapi"
	"github.com/elastic/elastic-transport-go/v8/elastictransport"
	"fmt"
	"strings"
	"encoding/json"
	"strconv"
	"time"
)
/*
--------------------------------------------------------------
	open interest doc format
--------------------------------------------------------------
*/
type open_interest_doc struct {
	Time time.Time `json:"Time"`
	OpenInterest float64
}
/*
--------------------------------------------------------------
	4chan doc format
--------------------------------------------------------------
*/
type doc_4chan struct {
	Title string `json:"title"`
	Link string `json:"link"`
	Replies int `json:"replies"`
	Time time.Time
}
/*
--------------------------------------------------------------
	sushi doc format
--------------------------------------------------------------
*/
type Stable_doc struct {
	Token_price float64
	Weth_price_usd float64
	Dollar_price_usd float64
	Router_address string
	Time time.Time
}

/*
--------------------------------------------------------------
	threadstore to stop duplicates format
--------------------------------------------------------------
*/
type threadstore struct {
	Time time.Time
	No int64
}
/*
--------------------------------------------------------------
	same as threadstore but stores id as a string instead of int64
--------------------------------------------------------------
*/
type thread_store_str struct {
	Time time.Time
	Id string
}

/*
--------------------------------------------------------------
	binance btc/usdt price
--------------------------------------------------------------
*/
type price_doc struct {
    Time time.Time `json:"Time"`
    Price float64
}
/*
--------------------------------------------------------------
	reddit doc format
--------------------------------------------------------------
*/
type reddit_doc struct {
	Title string `json:"title"`
	Num_comments int `json:"num_comments"`
	Selftext string `json:"selftext"`
	Url string `json:"url"`
	Time time.Time
}
/*
--------------------------------------------------------------
	tweet doc format
--------------------------------------------------------------
*/
type tweet_doc struct {
	User_name string
	User_screen_name string
	Tweet_Text string
	Tweet_created_at string
	Time time.Time
	Url string

}

/*
--------------------------------------------------------------
	structure to store ftx candle data, not used
--------------------------------------------------------------
*/
type ftx_candle struct {
	Close float64 
	High float64 
	Low float64 
	Open float64 
	StartTime time.Time 
	Volume float64 
  }

/* 
--------------------------------------------------------------
	make elasticsearch client pointing to localhost 
	params: none
	return: *elasticsearch.Client
	Example: tmp := make_es_client()
--------------------------------------------------------------
*/
func make_es_client() (*elasticsearch.Client) {

	cfg := elasticsearch.Config{
	  Addresses: []string{
		"http://localhost:9200",
		// "http://localhost:9201",
	  },
	  Username: "elastic",
	  Password: "",
	}
  
	es, err := elasticsearch.NewClient(cfg)
	if err != nil {
	  log.Fatalf("Error creating the client: %s", err)
	}
	return es
  }

/*
--------------------------------------------------------------
  params: *elasticsearch.Client, json 
  returns: void (prints result of operation)
  example: store_doc(doc, "test", cli)
--------------------------------------------------------------
*/

func store_doc(json_ interface{}, index string, cli *elasticsearch.Client) {
	
	docStr, err := json.Marshal(json_)
	if err != nil {
		fmt.Println("json.Marshal ERROR:", err)
	}
	// fmt.Println("fuck")
	res, err := cli.Index(index, strings.NewReader(string(docStr)))
	if err != nil {
		fmt.Println("ERROR:", err)
	}
	fmt.Println(res)
}


/* 
--------------------------------------------------------------
elastic search client 
used to create a connection to elastic search node
--------------------------------------------------------------
*/
type Client struct {
	*esapi.API          // Embeds the API methods
	Transport           elastictransport.Interface
	metaHeader          string
	compatibilityHeader bool

	disableMetaHeader   bool
	productCheckMu      sync.RWMutex
	productCheckSuccess bool
}

/*
--------------------------------------------------------------
create an index in the database 
params: *elasticsearch.Client, string
return: *esapi.Response
Example: create_index(cli, "test")
--------------------------------------------------------------
*/
func create_index(cli *elasticsearch.Client, name string) (*esapi.Response) {

	// s := []string{name}
	req := esapi.IndicesCreateRequest{
	  Index:      name,
	  Pretty:     true,
	  Human:      true,
	}
	res, err := req.Do(context.Background(), cli)
		if err != nil {
		  log.Fatalf("Error getting response: %s", err)
	}
  
	return res
}

/* 
--------------------------------------------------------------
return 10000 documents from requested index
params: *elasticsearch.Client (client object es), string (name of index in es database)
returns: map[string]interface{} (json, returns max amount 10000 docs)
example: search_get_all(cli, "puppies-index");
--------------------------------------------------------------
*/
func search_get_all(cli *elasticsearch.Client, index string) (map[string]interface{}) {
	var body = `{"query": {"match_all": {}}}`
	var indx = []string{index}
	req := esapi.SearchRequest{
		Index:      indx,
		Body: 		strings.NewReader(body),
		Size:    esapi.IntPtr(10000),
		Pretty:     true,
		Human:      true,	
	}

	res, err := req.Do(context.Background(), cli)
		if err != nil {
			log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()
	
	var resp map[string]interface{}

	if err := json.NewDecoder(res.Body).Decode(&resp); err != nil {
		log.Fatalf("Error parsing the response body: %s", err)
	}
	return resp
}



/* 
--------------------------------------------------------------
delete an index in the database
params: *elasticsearch.Client, string
return: *esapi.Response
Example: delete_index(cli, "test")
--------------------------------------------------------------
*/
func delete_index(cli *elasticsearch.Client, name string) (*esapi.Response) {
  
	s := []string{name}
	req := esapi.IndicesDeleteRequest{
	  Index:      s,
	}
	res, err := req.Do(context.Background(), cli)
		if err != nil {
		  log.Fatalf("Error getting response: %s", err)
	}
  
	return res
  }




/* 
--------------------------------------------------------------
store a doc in the es database
params: int64, string, *elasticsearch.Client
return: void
Example: store_thread_no(11213123, "test-index", cli);
--------------------------------------------------------------
*/
func store_thread_no(no int64, index string, cli *elasticsearch.Client) {
	doc := threadstore{}
	doc.Time = time.Now()
	doc.No = no
	threads_json, err := json.Marshal(doc)
	if err != nil {
		fmt.Println("error: ", err)
	}
	res, err := cli.Index(index, strings.NewReader(string(threads_json)))
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}
	fmt.Println(res)

}

/* 
--------------------------------------------------------------
search threads in the database
params: cli, int64, string
returns: bool
example: search_threads(cli, 11213123, "test-index")
--------------------------------------------------------------
*/
func search_threads(cli *elasticsearch.Client, no int64, index string) (bool) {

	var check bool
	var body = `{"query": {"bool": {"must": [{"match": {"No": "` + strconv.FormatInt(no, 10) + `"}}]}}}`
	var indx = []string{index}
	req := esapi.SearchRequest{
		Index:      indx,
		Body: 		strings.NewReader(body),
		Size:    esapi.IntPtr(10000),
		Pretty:     true,
		Human:      true,	
	}

	res, err := req.Do(context.Background(), cli)
		if err != nil {
			log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()
	
	var resp map[string]interface{}

	if err := json.NewDecoder(res.Body).Decode(&resp); err != nil {
		log.Fatalf("Error parsing the response body: %s", err)
	}
	hit := resp["hits"].(map[string]interface{})["total"].(map[string]interface{})["value"].(float64)

	if hit == 0 {
		check = false
	} else {
		check = true
	}

	return check
}



/*
--------------------------------------------------------------
params: cli
[]float64 (returns elastic search index storing thread numebrs to id them)
not being used
--------------------------------------------------------------
*/
func get_thread_store(cli *elasticsearch.Client) ([]float64) {
	tmp := search_get_all(cli, "threads-store")
	fmt.Println(tmp)
	var ret []float64
	for _, hit := range tmp["hits"].(map[string]interface{})["hits"].([]interface{}) {
		tmp2 := hit.(map[string]interface{})["_source"].(map[string]interface{})["No"]
		fmt.Println(tmp2)
		ret = append(ret, tmp2.(float64))
	}
	return ret
}