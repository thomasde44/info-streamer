package main

import (
	"net/http"
	"encoding/json"
	"log"
	"net/url"
	"strings"
	"github.com/elastic/go-elasticsearch/v8"
	"time"
	"sync"
	"hash/fnv"
)	
	

/*
--------------------------------------------------------------
	params: url string
	returns: string
	description: gets the bearer token from reddit for auth
--------------------------------------------------------------
*/
func get_bearer(api_url string) (string) {
	
	data := url.Values{}
	data.Set("grant_type", "password")
	data.Set("username", "username")
	data.Set("password", "password")
	client := &http.Client{
	}
	req, err := http.NewRequest("POST", api_url, strings.NewReader(data.Encode()))
	if err != nil {
		panic(err)
	}
	req.SetBasicAuth("token", "token")

	req.Header.Set("User-Agent", "fuck")
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode == 200 {
		var res map[string]interface{}

		if err := json.NewDecoder(resp.Body).Decode(&res); err != nil {
			log.Fatalf("Error parsing the response body: %s", err)
		}
		token := res["access_token"].(string)

		return token
	} else {
		return ""
	}

}
/*
--------------------------------------------------------------
	params: *elasticsearch.Client, string, string, string
	returns: void
	example: record_subreddit_doc(cli, bearToken, "r/Drugs/new", "test-index")
	description: gets the response from reddit
--------------------------------------------------------------
*/
func record_subreddit_doc(cli *elasticsearch.Client, bearer string, subreddit string, es_index string) {
	
	hdr := http.Header{
		"Authorization": []string{"Bearer " + bearer},
		"User-Agent": []string{"fuck"},
	}
	resp, err := do_http_get("https://oauth.reddit.com/" + subreddit, hdr)
	if err != nil {
		panic(err)
	}
	if resp.StatusCode == 200 {

		var res map[string]interface{}

		if err := json.NewDecoder(resp.Body).Decode(&res); err != nil {
			log.Fatalf("Error parsing the response body: %s", err)
		}
		
		data := res["data"].(map[string]interface{})["children"].([]interface{}) 
		for _, v := range data {
			thread := v.(map[string]interface{})["data"].(map[string]interface{})["id"].(string)
			thread_no := int(hash(thread))
			if search_threads(cli, int64(thread_no), "threads-store-reddit") == false {
				if v.(map[string]interface{})["data"].(map[string]interface{})["num_comments"].(float64) >= 5 {
					store_thread_no(int64(thread_no), "threads-store-reddit", cli)
					doc := reddit_doc{}
					doc.Title = v.(map[string]interface{})["data"].(map[string]interface{})["title"].(string)
					doc.Selftext = v.(map[string]interface{})["data"].(map[string]interface{})["selftext"].(string)
					doc.Url = v.(map[string]interface{})["data"].(map[string]interface{})["url"].(string)
					doc.Time = time.Now()
					store_doc(doc, es_index, cli)

				}
			}
		}
	}
}

/*
--------------------------------------------------------------
	indice names in es database
--------------------------------------------------------------
*/
const math_ = "reddit-mathematics"
const lin_alg_ = "reddit-linearalgebra"
const drugs_ = "reddit-drugs"
const defi_ = "reddit-defi"


/*
--------------------------------------------------------------
	params: cli *elasticsearch.Client
	returns: void
	example: store_reddit_documents(cli)
--------------------------------------------------------------
*/
func store_reddit_doctument(cli *elasticsearch.Client) {
	var	wg sync.WaitGroup
	var urls = []string{} // change to reddit r/subreddit/new
	var index_names = []string{drugs_, math_, lin_alg_, defi_} // change to reddit
	
	t := get_bearer("https://www.reddit.com/api/v1/access_token")
	if t != "" {
		for i, index := range index_names {
			wg.Add(1)

			go func(cli *elasticsearch.Client, i int, index string) {
				defer wg.Done()
				record_subreddit_doc(cli, t, urls[i], index)
			}(cli, i, index)
			wg.Wait()
		}
	}
}

/*
--------------------------------------------------------------
	make id to stop duplicates
	params: string of thread
	returns: uint32
--------------------------------------------------------------
*/
func hash(s string) uint32 {
        h := fnv.New32a()
        h.Write([]byte(s))
        return h.Sum32()
}





