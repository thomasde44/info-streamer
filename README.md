## <img src="docs/happy.png"  width="60" height="60"> About The Project 

**The project is a space to experiment with software and collect information 
that I am interested in keeping track of. At the moment the 
workers collecting information are communicating to the database like below.**<br />

<img src="docs/diagram.jpg"><br />

**The purpose of the project is to have fun and experiment with using different protocols, collecting and 
formatting information in a way that is usable for computation.**<br />

**Each highlighted topic is running as it's own go routine. The routines execute on a timer and intervals 
are based approximately on current rate limits of the remote server or based on subjects rate of information
issuance.**<br />

**One of the major focuses and goal of this is to have a project that runs 24/7 so the intention
of this program is to continously run.**<br />

---

## <img src="docs/cheeky.png"  width="20" height="20"> Getting Started<br />



#### <img src="docs/silly.png"  width="35" height="35"> install elasticsearch <br />

---
*This is meant to be installed for ubuntu 20.04 but could work on any debian based distro.*

`wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -`

`sudo apt-get install apt-transport-https`

`echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list`

`sudo apt-get update && sudo apt-get install elasticsearch`

---

### <img src="docs/tall.png"  width="30" height="30">  start elascticsearch <br />

---

Start Elasticsearch and run it in the background. <br />

`sudo systemctl start elasticsearch`

`sudo systemctl enable elasticsearch`

### <img src="docs/funny.png"  width="30" height="30"> elacticsearch security settings <br />

---



Unless you pointed the installation directory to another location your elasticsearch will be installed in
*/usr/share/elasticsearch* and you config files will be in */etc/elasticsearch/*. Add the following to your config.<br />

`xpack.security.enabled: true`

`discovery.type: single-node`<br />



Then run the following command.<br />

`./bin/elasticsearch-setup-passwords interactive`<br />

This will prompt you to set the following passwords.

```
xpack.security.enabled: true
discovery.type: single-node
Enter password for [elastic]: 
Reenter password for [elastic]: 
Enter password for [apm_system]: 
Reenter password for [apm_system]: 
Enter password for [kibana_system]: 
Reenter password for [kibana_system]: 
Enter password for [logstash_system]: 
Reenter password for [logstash_system]: 
Enter password for [beats_system]: 
Reenter password for [beats_system]: 
Enter password for [remote_monitoring_user]: 
username: elastic
```
<br />

Once That is finished restart the process. <br />

`sudo systemctl restart elasticsearch` <br />

You can also set the ip address and port for the service to listen on in the yaml file. More information can be found here <a href="https://www.elastic.co/guide/en/elasticsearch/reference/current/settings.html">info</a>.

---

#### <img src="docs/strong.png"  width="25" height="25"> install kibana <br />

---

Kibana is part of the elk stack and used as a dashboard for visualization of the data. To install run the commands below in the terminal. <br />

`wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -`<br />

`sudo apt-get install apt-transport-https`<br />

`echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list`<br />

`sudo apt-get update && sudo apt-get install kibana`<br />

---

### <img src="docs/worry.png"  width="30" height="30">  start kibana <br />

---

Start Kibana and run it in the background. <br />

`sudo systemctl enable kibana`<br />

`sudo systemctl start kibana`<br />


Similar for the elasticsearch setup. Kibana is installed by default into your **/etc/kibana** directory.
Within the directory it contains the file **kibana.yml**. <br />
Add the following line to it:

`elasticsearch.username: "kibana_system"`<br />

This is the default username for elasticsearch and identifies the associated password that was set in the users
setup of elasticsearch. If your username was different in the yaml file obviously set it to that one. By default kibana is configured to connect to elasticsearch's default port number 9200. <br />

After in default install directory. */usr/share/kibana/* run the following. <br />

`sudo ./bin/kibana-keystore create`<br />

`sudo ./bin/kibana-keystore add elasticsearch.password` <br />

The second will ask you to enter your password, this is the one you previously set while setting up kibana, the elastic user password. Now restart 
kibana and log in through the browser <br />

`sudo systemctl restart kibana`<br />

---

### <img src="docs/bump.png"  width="30" height="30">  building and running <br />

---

The main files of the program are located in **./go-files**. To build the binary for the main program type the command in the terminal. <br />

`go build -o main go-files/*` <br />

This will produce a binary called main, if you want to name it something different change the name after the -o flag with the compiler command. <br />

Running the binary from terminal. <br />

`./main` <br />

Example of the logging in the terminal while the program is running. This is all the documents being stored into the database.

<img src="docs/log.jpg"> <br />

Debug build to enable gdb debugger<br />

`go build -o main -gcflags "-N" go-files/*` <br />

Test the installation <br />

`cd go-files`

`go test`

```
PASS
ok  	info-streamer/go-files	0.800s
```
---

## <img src="docs/messy.png"  width="30" height="30"> Project Details <br />

The project utilizes Elasticsearch database to store the data. You can think of elasticsearch like 
your normal unix file systems, where directories are called indexes and files are all called documents. Documents 
have a json like structure **{label: obj, label2: obj2}** and can be any size. Each data point has its own index and associated document. 
An example of the document for stable coin prices is below. The document structures and other elasticsearch functions are located in **go-files/cli_elastic.go** <br />

```
type Stable_doc struct {
	Token_price float64
	Weth_price_usd float64
	Dollar_price_usd float64
	Router_address string
	Time time.Time
}
```
Each go file in *./go-files/* contains one or more storing function. Generally titled **store_*_document()**. These functions are the main workers for communicating with endpoints and collecting the data. <br />

*from go-files/cli_reddit.go*

```
const math_ = "reddit-mathematics"
const lin_alg_ = "reddit-linearalgebra"
const defi_ = "reddit-defi"

var index_names = []string{math_, lin_alg_, defi_}
```

*from go-files/cli_eth_stables.go*
```
indices := []string{"sushi-stables-eth-usdt", "sushi-stables-eth-usdc", "sushi-stables-dai", "uni-stables-eth-usdt", "uni-stables-eth-usdc", "uni-stables-dai"}
```
Depending on the amount of data points each one will have a corresponding index in elasticsearch. When more than one the storing fucntions 
loop through using go routines to collect the data and store it in the appropriate index.<br />

The intention over time is to generalize it so that each section is easily extendable to add new information to be tracked.<br />

Viewing in Kibana, depending on the ip and port you assigned in the config but is generally the loopback and port 9200. You can access it through your
browser by logging in with you username and password. You can search for and query specific information to view in your dashboard. An example of how you can view the information you have collected is below.

<img src="docs/kibana1.jpg"> <br />

<img src="docs/kibana2.jpg"> <br />

On the http endpoint of your database you can also submit POST/GET requests to query all or some of the information of an index. 
The example structure of a query is seen here.<br />

```
curl -X GET "localhost:9200/_search?pretty" -H 'Content-Type: application/json' -d'
{
  "query": {
    "query_string": {
      "query": "(new york city) OR (big apple)",
      "default_field": "content"
    }
  }
}
'
```

There are sdk's for most languages to query, more information on the structure and how to isolate specific data 
you are searching for can be found <a href="https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html"> here. </a>

